<?php

use jw\routing\Base;

class Routing extends Base
{
  protected function configure()
  {
    $this->add('~^/$~', 'info', 'home');
    $this->add('~^/portfolio$~', 'info', 'portfolio');
    $this->add('~^/prices$~', 'info', 'prices');
    $this->add('~^/contact$~', 'contact', 'email');
    $this->add('~^/trytrytry/(?P<site>.*)~', 'external', 'trytrytry');
  }
}
