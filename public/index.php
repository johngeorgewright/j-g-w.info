<?php

require_once dirname(__FILE__).'/../vendor/jw/util/Autoloader.php';

use jw\util\Autoloader;
use jw\controller\Base as Controller;

Autoloader::register();
Autoloader::registerDir(realpath(dirname(__FILE__).'/../'));

$controller = new Controller(Configuration::getInstance());
$controller->dispatch();

