YUI.add("jw-description-overlay", function(Y)
{
  "use-strict";

  var DescriptionOverlay = Y.Base.create("JwDescriptionOverlay", Y.View, [], {
        
        events: {
          ".jw-description-overlay-item": {
            mouseenter: "mouseenter",
            mouseleave: "mouseleave"
          }
        },

        originalStyles: {},

        initializer: function(config)
        {
          if (!config.container)
          {
            throw new Error("You must supply a container.");
          }
          else if (Y.Lang.isString(config.container))
          {
            this.container = Y.one(config.container);
          }
        },

        render: function()
        {
          var overlays = this.container.all(".jw-description-overlay"),
              items    = this.container.all(".jw-description-overlay-item");

          items.setStyle("position", "relative");

          overlays.removeClass("yui3-loading");

          overlays.each(function(overlay)
          {
            this.originalStyles[overlay.generateID()] = {
              height        : overlay.get("offsetHeight")+"px",
              paddingTop    : overlay.getStyle("paddingTop"),
              paddingBottom : overlay.getStyle("paddingBottom")
            };
          }, this);

          overlays.setStyles({
            position      : "absolute",
            bottom        : 0,
            height        : 0,
            overflow      : "hidden",
            paddingTop    : 0,
            paddingBottom : 0
          });
        },

        mouseenter: function(e)
        {
          var overlay = e.currentTarget.one(".jw-description-overlay");
          overlay.transition(this.originalStyles[overlay.get("id")]);
        },

        mouseleave: function(e)
        {
          e.currentTarget.one(".jw-description-overlay").transition({
            height        : 0,
            paddingTop    : 0,
            paddingBottom : 0
          });
        }

      });

  Y.namespace("JW.View").DescriptionOverlay = DescriptionOverlay;

}, "0.0.1", {
  requires: ["base", "event-mouseenter", "transition", "view"]
});

