YUI.GlobalConfig = {
  groups: {
    jw: {
      base: "/js/modules/",
      modules: {
        "jw-description-overlay": {
          path: "description-overlay/description-overlay.js",
          requires: ["base", "event-mouseenter", "transition", "view"]
        }
      }
    }
  }
};

