<?php

require_once 'Twig/Autoloader.php';
require_once '../JW/Base.php';
require_once '../JW/FileSystem.php';

use JW\FileSystem;

Twig_Autoloader::register();

$loader = new Twig_Loader_Filesystem(FileSystem::get_instance()->template_dirs);
$twig   = new Twig_Environment($loader);

echo $twig->render('test.html', array('name' => 'Mungface'));

