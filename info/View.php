<?php

namespace info;

use jw\view\Base;
use jw\view\HttpShortcuts;
use jw\request\Http as HttpRequest;

class View extends Base
{
  protected function mixins()
  {
    $this->mixin(new HttpShortcuts($this));
  }

  public function home(HttpRequest $request)
  {
    setcookie('mung', 'face');
    return $this->renderTemplate('info/home.php', array(
      'email' => $this->controller->getConfiguration()->get('email_to')
    ));
  }

  public function portfolio(HttpRequest $request)
  {
    return $this->renderTemplate('info/portfolio.php');
  }

  public function prices(HttpRequest $request)
  {
    return $this->renderTemplate('info/prices.php', array(
      'email' => $this->controller->getConfiguration()->get('email_to')
    ));
  }
}

