<?php

namespace external;

use jw\view\Base;
use jw\request\Http as HttpRequest;
use jw\response\http\Html as HtmlResponse;

class View extends Base
{
  /**
   * A request that will keep trying a URL
   * until it suceeds. Great for Glasto registration.
   *
   * @param jw\request\Http $request
   * @return jw\response\http\HTML
   */
  public function trytrytry(HttpRequest $request)
  {
    $curl = new Request();
    $curl->setUrl($request->site);
    $curl->trytrytry();
    $response = $curl->getResponse();
    $cookies  = $this->render('external/trytrytry/cookies.php', array('cookies' => $curl->getCookies()));
    $response = preg_replace('~</body>~im', $cookies.'</body>', $response);
    return new HtmlResponse($response);
  }
}
