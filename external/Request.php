<?php

namespace external;

/**
 * Creates a request for external sites.
 *
 * @author John Wright <johngeorge.wright@gmail.com
 * @package external
 */
class Request
{
  /**
   * The request handler (in this case cURL)
   *
   * @var cURL resource
   */
  private $handler;
  
  /**
   * The URL.
   *
   * @var string
   */
  private $url;
  
  /**
   * The cookie data.
   *
   * @var string
   */
  private $cookies=array();
  
  private $response;
  
  /**
   * Constructor.
   *
   */
  public function __construct()
  {
    $this->handler = curl_init();
    $this->setOpts(array(
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_MAXREDIRS      => 3,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_USERAGENT      => $_SERVER['HTTP_USER_AGENT'],
      CURLOPT_HEADER         => true
    ));
  }
  
  /**
   * Destructor.
   *
   */
  public function __destruct()
  {
    curl_close($this->handler);
  }
  
  /**
   * Returns the URL.
   *
   * @return string
   */
  public function getUrl()
  {
    return $this->url;
  }
  
  /**
   * Returns all the coookies as a string.
   *
   * @return string
   */
  public function getCookies()
  {
    return $this->cookies;
  }
  
  /**
   * Returns the response.
   *
   * @return string
   */
  public function getResponse()
  {
    return $this->response;
  }
  
  /**
   * Sets the URL.
   *
   * This is required before executing.
   *
   * @param string $url
   */
  public function setUrl($url)
  {
    $this->url = $url;
    $this->setOpt(CURLOPT_URL, $url);
  }
  
  /**
   * Sets a cURL option.
   *
   * @param integer $key
   * @param mixed $val
   */
  public function setOpt($key, $val)
  {
    curl_setopt($this->handler, $key, $val);
  }
  
  /**
   * Sets an array of curl options.
   *
   * @param mixed[] $options
   */
  public function setOpts($options)
  {
    curl_setopt_array($this->handler, $options);
  }
  
  /**
   * Keeps executing the request until it receives a
   * successfull response.
   *
   */
  public function trytrytry()
  {
    // Make sure PHP doesn't time out.
    set_time_limit(99999999);
    
    // Try again if we get no response in 30secs.
    $this->setOpt(CURLOPT_TIMEOUT, 30);
    
    do
    {
      $response = $this->exec();
    } 
    while(curl_errno($this->handler) > 0);
  }
  
  /**
   * Executes the request and returns the response.
   *
   */
  public function exec()
  {
    if (!$this->url)
    {
      throw new Exception('Requires a URL.');
    }
    
    $response = curl_exec($this->handler);
    
    if ($response)
    {
      $this->parseResponse($response);
    }
  }
  
  /**
   * Parses a HTTP response stores the data in this object.
   *
   * @param string $response
   */
  private function parseResponse($response)
  {    
    if (preg_match_all('/^Set-Cookie: (.*)$/m', $response, $matches, PREG_SET_ORDER))
    {
      foreach ($matches as $match)
      {
        $cookie = array(
          'name'      => null,
          'value'     => null,
          'expires'   => null,
          'path'      => null,
          'domain'    => null,
          'http_only' => null
        );
      
        if (preg_match('/^(.*)=(.*)(?:;.*)?$/U', $match[1], $cookie_value))
        {
          $cookie['name']  = $cookie_value[1];
          $cookie['value'] = $cookie_value[2];
          
          if (preg_match('/expires=(.*)(?:;.*)?$/U', $match[1], $cookie_expires))
          {
            $cookie['expires'] = $cookie_expires[1];
          }
          
          if (preg_match('/path=(.*)(?:;.*)?$/U', $match[1], $cookie_path))
          {
            $cookie['path'] = $cookie_path[1];
          }
          
          if (preg_match('/domain=(.*)(?:;.*)?$/U', $match[1], $cookie_domain))
          {
            $cookie['domain'] = $cookie_domain[1];
          }
          
          if (preg_match('/HttpOnly/', $match[1], $cookie_http_only))
          {
            $cookie['http_only'] = !empty($cookie_http_only);
          }
          
          $this->cookies[] = $cookie;
        }
      }
    }
    
    if (preg_match('/^(<!doctype.*)/ims', $response, $text))
    {
      $body = $text[1];
    }
    
    // Add the <base/> tag to the header to make sure 
    // all links and images are found on the site's 
    // server and not mine.
    $this->response = preg_replace('~<head([^>]*)>~i', '<head$1><base href="'.$this->url.'" />', $body);
  }
}
