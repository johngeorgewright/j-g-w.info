<div class="yui3-u-1-3">
	<div class="jw-info-portfolio-item jw-description-overlay-item"
		id="jw-info-portfolio-item-<?php echo $id ?>">
		<h3 class="jw-info-portfolio-item-heading">
		<?php echo $title ?></h3>

		<div class="jw-info-portfolio-item-image">
			<a target="_blank" href="<?php echo $url ?>"><img
				src="/images/portfolio/<?php echo $image ?>"
				alt="<?php echo $title ?> logo" />
			</a>
		</div>

		<div
			class="jw-info-portfolio-item-description jw-description-overlay yui3-loading">
			<dl class="yui3-g">
				<dt class="yui3-u-1-3">What is it?</dt>
				<dd class="yui3-u-2-3">
				<?php echo $description ?></dd>

				<dt class="yui3-u-1-3">Technologies</dt>
				<dd class="yui3-u-2-3">
					Developed using
					<?php echo $technologies ?>
					.
				</dd>

				<dt class="yui3-u-1-3">URL</dt>
				<dd class="yui3-u-2-3">
					<a href="<?php echo $url ?>" target="_blank"><?php echo $url ?>
					</a>
				</dd>
			</dl>
		</div>
	</div>
</div>

