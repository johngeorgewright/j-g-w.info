<?php $this->extend('base.php') ?>

<?php $this->block('content') ?>
<h2 id="jw-info-home-title">
	Web Developer / <abbr title="Search Engine Optimisation">SEO</abbr>
	Webmaster / <abbr title="User Experience">UX</abbr> Designer
</h2>

<p class="jw-info-important">Sepcialising in profitable, commercial and
	communcation solutions for your unique business requirements.</p>

<p>Hi there,</p>
<p>My name is John Wright and I'm a freelance Web Developer. I've had
	over 4 years industry experience with web sites large and small. I'm
	senior in both backend and frontend development and I'm highly skilled
	in search engine optimisation helping you get the right customers to
	your site.</p>
<p>
	If you'd like to hire me, have any enquiries or just fancy a chat,
	please don't hesitate to <a href="mailto:<?php echo $email ?>">contact
		me</a>.
</p>

<?php $this->endBlock() ?>

