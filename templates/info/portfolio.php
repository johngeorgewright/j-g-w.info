<?php $this->extend('base.php') ?>

<?php $this->block('page_title', 'Portfolio | John Wright') ?>
<?php $this->block('page_description', 'Here\'s a small selection of projects I have worked on during my career.') ?>

<?php $this->block('content') ?>
<h2>My Portfolio</h2>
<p>Here's a small selection of projects I have worked on during my
	career.</p>

<div class="yui3-g jw-info-portfolio-items clearfix">


<?php $this->incl('info/portfolio/item.php', array(
      'id'           => 'tr',
      'title'        => 'TrustedReviews',
      'image'        => 'tr.png',
      'url'          => 'http://www.trustedreviews.com',
      'description'  => 'A site devoted to reviewing technology.',
      'technologies' => 'Symfony (PHP), MySQL, Apache, YUI, Pluck'
)) ?>



<?php $this->incl('info/portfolio/item.php', array(
      'id'           => 'khadi',
      'title'        => 'Khadi Papers',
      'image'        => 'khadi.jpg',
      'url'          => 'http://www.khadipapers.com',
      'description'  => 'Handmade paper for artists.',
      'technologies' => 'Actinic (PHP e-commerce), Apache'
)) ?>



<?php $this->incl('info/portfolio/item.php', array(
      'id'           => 'zs',
      'title'        => 'Zimmer Stewart Gallery',
      'image'        => 'zs.gif',
      'url'          => 'http://www.zimmerstewart.co.uk',
      'description'  => 'A contemporary art gallery in Arundel.',
      'technologies' => 'PHP, Smarty, MySQL, Apache'
)) ?>
</div>

<div class="yui3-g jw-info-portfolio-items clearfix">


<?php $this->incl('info/portfolio/item.php', array(
      'id'           => 'partimi',
      'title'        => 'Partimi',
      'image'        => 'partimi.png',
      'url'          => 'http://www.partimi.com',
      'description'  => 'An independant fashion label based in London.',
      'technologies' => 'PHP, MySQL, Apache, jQuery'
)) ?>



<?php $this->incl('info/portfolio/item.php', array(
      'id'           => 'gh',
      'title'        => 'Graeme Hunt',
      'image'        => 'gh.png',
      'url'          => 'http://www.graemehunt.com',
      'description'  => 'A business selling classic cars.',
      'technologies' => 'PHP, SQLite, Apache, YUI'
)) ?>


<?php $this->incl('info/portfolio/item.php', array(
      'id'           => 'ispolson',
      'title'        => 'I.S. Polson',
      'image'        => 'ispolson.png',
      'url'          => 'http://www.ispolson.com',
      'description'  => 'A business restoring and selling Roesch designed Talbots.',
      'technologies' => 'PHP, SQLite, Apache, YUI'
)) ?>
</div>

<div class="yui3-g jw-info-portfolio-items clearfix">


<?php $this->incl('info/portfolio/item.php', array(
      'id'           => 'ag',
      'title'        => 'The Air Gallery',
      'image'        => 'ag.png',
      'url'          => 'http://www.airgallery.co.uk',
      'description'  => 'An art gallery in Mayfair.',
      'technologies' => 'PHP, SQLite, Apache, YUI'
)) ?>



<?php $this->incl('info/portfolio/item.php', array(
      'id'           => 'tangmere',
      'title'        => 'Tangmere Military Aviation Museum',
      'image'        => 'tangmere.jpg',
      'url'          => 'http://www.tangmere-museum.org.uk',
      'description'  => 'An historic aircraft museum.',
      'technologies' => 'PHP, Smarty, MySQL, Apache'
)) ?>



<?php $this->incl('info/portfolio/item.php', array(
      'id'           => 'rdi',
      'title'        => 'Rancho del Ingles',
      'image'        => 'rdi.jpg',
      'url'          => 'http://www.ranchodelingles.com',
      'description'  => 'A hotel in rural Spain.',
      'technologies' => 'PHP, Apache'
)) ?>

</div>

<div class="yui3-g jw-info-portfolio-items clearfix">


<?php $this->incl('info/portfolio/item.php', array(
      'id'           => 'tippingpoint',
      'title'        => 'Tipping Point',
      'image'        => 'tippingpoint.jpg',
      'url'          => 'http://www.tippingpoint.org.uk',
      'description'  => 'TippingPoint is network-based organisation aiming to be a year round ‘connector’ of the arts and climate science worlds.',
      'technologies' => 'PHP, WordPress, MySQL, jQuery'
)) ?>


<?php $this->incl('info/portfolio/item.php', array(
      'id'           => 'frt',
      'title'        => 'FRT',
      'image'        => 'frt.png',
      'url'          => 'http://code.google.com/p/frt/',
      'description'  => 'A FTP project releasing tool.',
      'technologies' => 'Bash'
)) ?>


</div>

<?php $this->endBlock() ?>

<?php $this->block('javascript') ?>
YUI().use("jw-description-overlay", function(Y) { new
Y.JW.View.DescriptionOverlay({container:
Y.one("#jw-content")}).render(); });
<?php $this->endBlock() ?>

