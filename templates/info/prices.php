<?php $this->extend('base.php') ?>

<?php $this->block('page_title', 'Prices | John Wright') ?>

<?php $this->block('content') ?>
<h2>Prices</h2>

<p>
	Quotes are generally dependant on the size of the project. My freelance
	work is also a hobby and therefore I can be very competitive with
	prices. <a href="mailto:<?php echo $email ?>">Get in contact</a> if
	you'd like a quote.
</p>

<h3>Social Networking</h3>
<p class="jw-info-important">
	Too busy to keep up together with all the social networking platforms
	like Facebook, Google plus, Twitter etc? I'm offering to do all that
	for you for a measly &pound;5 per week. I'll make sure that you have
	daily updates keeping your followers happy and your business thriving.
	<a href="mailto:<?php echo $email ?>">Email me</a> for more details.
</p>

<?php $this->endBlock() ?>

