<?if(!empty($cookies)):?>
  <div style="background-color:#000000; color:#FFFFFF; font-family:verdana; padding:10px;">
    <h2 style="font-size:18px; font-weight:bold; margin-bottom:1em;">You need to manually add the following cookies</h2>
    <table style="width:100%; font-size:12px;">
      <thead>
        <tr>
          <th style="border:1px solid #CCCCCC; padding:10px; font-weight:bold;">Name</th>
          <th style="border:1px solid #CCCCCC; padding:10px; font-weight:bold;">Value</th>
          <th style="border:1px solid #CCCCCC; padding:10px; font-weight:bold;">expires</th>
          <th style="border:1px solid #CCCCCC; padding:10px; font-weight:bold;">path</th>
          <th style="border:1px solid #CCCCCC; padding:10px; font-weight:bold;">domain</th>
          <th style="border:1px solid #CCCCCC; padding:10px; font-weight:bold;">http_only</th>
        </tr>
      </thead>
      <tbody>
        <?foreach($cookies as $cookie):?>
          <tr>
            <td style="border:1px solid #CCCCCC; padding:10px;"><?=$cookie['name']?></td>
            <td style="border:1px solid #CCCCCC; padding:10px;"><?=$cookie['value']?></td>
            <td style="border:1px solid #CCCCCC; padding:10px;"><?=$cookie['expires']?></td>
            <td style="border:1px solid #CCCCCC; padding:10px;"><?=$cookie['path']?></td>
            <td style="border:1px solid #CCCCCC; padding:10px;"><?=$cookie['domain']?></td>
            <td style="border:1px solid #CCCCCC; padding:10px;"><?=$cookie['http_only'] ? 'yes' : 'no'?></td>
          </tr>
        <?endforeach?>
      </tbody>
    </table>
  </div>
<?endif?>
