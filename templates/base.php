<!DOCTYPE html>
<html lang="en">
<head>
<title><?php $this->block('page_title', 'John Wright | Web Developer | SEO Webmaster | UX Designer') ?>
</title>
<link rel="stylesheet" type="text/css"
	href="http://yui.yahooapis.com/combo?3.4.1/build/cssreset/cssreset-min.css&amp;3.4.1/build/cssfonts/cssfonts-min.css&amp;3.4.1/build/cssgrids/cssgrids-min.css&amp;3.4.1/build/cssbase-context/cssbase-context-min.css" />
<!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
<link rel="stylesheet" type="text/css" href="/css/main.css" />
<link rel="stylesheet" type="text/css" href="/css/print.css"
	media="print" />
<meta name="keywords"
	content="<?php $this->block('page_keywords', 'John,Wright,web,developer,SEO,webmaster,search engine,ux,design') ?>" />
<meta name="description"
	content="<?php $this->block('page_description', 'Sepcialising in profitable, commercial and communcation solutions for your unique business requirements.') ?>" />
<meta name="robots" content="index,follow" />
<meta charset="utf-8" />
<script src="http://yui.yahooapis.com/3.4.1/build/yui/yui-min.js"></script>
<script src="/js/module-config.js"></script>
</head>
<body>
	<ul class="jw-ah">
		<li><a href="#jw-nav">Skip to navigation</a></li>
		<li><a href="#jw-content">Skip to content</a></li>
	</ul>
	<div id="jw-wrapper" class="jw-light">
		<div id="jw-header" class="jw-dark">
			<h1 id="jw-logo">
				<abbr title="John Wright"><span class="jw-logo-tag-delim">&lt;</span>j<span
					class="jw-logo-tag-delim">&gt;&lt;/</span>w<span
					class="jw-logo-tag-delim">&gt;</span> </abbr>
			</h1>
		</div>
		<div id="jw-content" class="yui3-cssbase jw-light">
			
			
		<?php $this->block('content') ?>
			
		<?php $this->endBlock() ?>
		</div>
		<div id="jw-nav" class="jw-dark">
			<ul>
				<li
					
				<?php if ($this->isRoute('info', 'home')) echo ' class="jw-active"' ?>>
					<a href="<?php echo $this->url('info', 'home') ?>">Home</a>
				</li>
				<li
					
				<?php if ($this->isRoute('info', 'portfolio')) echo ' class="jw-active"' ?>>
					<a href="<?php echo $this->url('info', 'portfolio') ?>">Portfolio</a>
				</li>
				<li
					
				<?php if ($this->isRoute('info', 'prices')) echo ' class="jw-active"' ?>>
					<a href="<?php echo $this->url('info', 'prices') ?>">Prices</a>
				</li>
				<li><a href="mailto:johngeorge.wright@gmail.com">Contact</a> <!--<a href="<?php echo $this->url('contact', 'email') ?>">Contact</a>-->
				</li>
			</ul>
		</div>
	</div>
	<script>
      <?php $this->block('javascript') ?><?php $this->endBlock() ?>
    </script>


</body>

</html>

