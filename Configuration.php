<?php

use jw\configuration\Base;

class Configuration extends Base
{
  protected function configure()
  {
    $this->root_dir        = dirname(__FILE__);
    $this->template_class  = 'jw\view\template\PHP';
    $this->template_dirs   = array($this->get('root_dir').DIRECTORY_SEPARATOR.'templates');
    $this->public_dir      = $this->get('root_dir').DIRECTORY_SEPARATOR.'public';
    $this->email_to        = 'johngeorge.wright@gmail.com';
    $this->enabled_modules = array('info', 'contact', 'external');
  }
}

